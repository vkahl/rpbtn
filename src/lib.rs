//! A crate to simplify using gpio buttons in single board computer applications

const DEFAULT_HOLD_PRECISION: u64 = 10;

use std::{collections::HashMap, fmt, marker::PhantomData, thread, time::Duration};

pub use rppal::gpio::{Error, Level};
use rppal::gpio::{Gpio, InputPin, Trigger};

mod button;
mod channel;

pub use button::*;
pub use channel::EventEmitter;

#[derive(Debug, Clone, Copy)]
pub struct ButtonEvent<B: 'static + Copy + Send + fmt::Debug> {
    pub id: B,
    pub pushed: bool,
    pub held: bool,
    pub double: bool,
}

pub struct ButtonWatcher<B, S, E> {
    hold_precision: Duration,
    sender: S,
    button_map: HashMap<u8, Button<B>>,
    gpio: Gpio,
    pins: Vec<InputPin>,
    _phantom: PhantomData<E>,
}

impl<B, S, E> ButtonWatcher<B, S, E>
where
    B: 'static + Copy + Send + fmt::Debug,
    S: crate::channel::EventEmitter<E> + Send,
    E: From<ButtonEvent<B>> + Send,
{
    /// Construct a new [`ButonWatcher`]
    ///
    /// # Errors
    ///
    /// if constructing the `Gpio` via the `rppal` crate fails.
    pub fn new(sender: S) -> Result<Self, Error> {
        Ok(Self {
            hold_precision: Duration::from_millis(DEFAULT_HOLD_PRECISION),
            sender,
            button_map: HashMap::default(),
            gpio: Gpio::new()?,
            pins: Vec::new(),
            _phantom: PhantomData,
        })
    }

    #[inline]
    #[must_use]
    pub fn hold_precision(mut self, hold_precision: Duration) -> Self {
        self.hold_precision = hold_precision;
        self
    }

    /// Add a button that shall be watched
    ///
    /// # Errors
    ///
    /// if setting the interrupt for this button fails.
    pub fn button(mut self, pin: u8, button: Button<B>) -> Result<Self, Error> {
        let mut input_pin = self.gpio.get(pin)?.into_input();
        input_pin.set_interrupt(Trigger::Both, None)?;
        // `input_pin` has successfully been initialized, so it can be added to
        // the `pins` vector and `button_map`.
        self.pins.push(input_pin);
        self.button_map.insert(pin, button);
        Ok(self)
    }

    /// Add multiple similar buttons that only differ in their pins and ids
    ///
    /// # Errors
    ///
    /// if setting the interrupt for one of the buttons fails. All buttons up to
    /// that point will be added anyway!
    pub fn buttons(mut self, prototype: &Button<B>, pins: &[u8], ids: &[B]) -> Result<Self, Error> {
        for (pin, id) in pins.iter().zip(ids) {
            let mut button = prototype.clone();
            button.id = *id;
            self = self.button(*pin, button)?;
        }
        Ok(self)
    }

    /// Block while watching buttons and emitting events into the sender
    #[allow(clippy::missing_panics_doc)]
    pub fn watch(self) {
        let ButtonWatcher { hold_precision, mut sender, mut button_map, gpio, pins, _phantom } =
            self;
        let pins_references: Vec<_> = pins.iter().collect();

        while let Ok(interrupt) =
            gpio.poll_interrupts(&pins_references, false, Some(hold_precision))
        {
            if let Some(interrupt) = interrupt {
                // Interrupt.
                let pin = interrupt.0.pin();
                let level = interrupt.0.is_high().into();
                let button = button_map.get_mut(&pin).expect("`button_map` contains `pin`");
                if button.handle_pushing(&mut sender, level).is_err() {
                    // The event receiver must have been dropped.
                    break;
                }
            } else {
                // Timeout.
                if Self::handle_holding(&mut button_map, &mut sender).is_err() {
                    // The event receiver must have been dropped.
                    break;
                }
            }
        }
    }

    fn handle_holding(
        button_map: &mut HashMap<u8, Button<B>>, sender: &mut S,
    ) -> Result<(), S::EmitError> {
        for button in button_map.values_mut() {
            button.handle_holding(sender)?;
        }
        Ok(())
    }
}

impl<B, S, E> ButtonWatcher<B, S, E>
where
    B: 'static + Copy + Send + fmt::Debug,
    S: 'static + crate::channel::EventEmitter<E> + Send,
    E: 'static + From<ButtonEvent<B>> + Send,
{
    pub fn watch_background(self) -> thread::JoinHandle<()> {
        thread::spawn(move || {
            self.watch();
        })
    }
}
