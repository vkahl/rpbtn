use std::{
    fmt,
    time::{Duration, Instant},
};

pub use rppal::gpio::Level;

use crate::{ButtonEvent, EventEmitter};

const DEFAULT_DEBOUNCE: u64 = 100;

#[derive(Debug, Clone)]
pub struct Button<B> {
    pub id: B,
    pub active_level: Level,
    pub hold_time: Option<Duration>,
    pub double_time: Duration,
    pub debounce: Duration,
    switch_time: Instant,
    level: Level,
    level_debounced: Level,
    held: bool,
    double: bool,
}

impl<B: 'static + Copy + Send + fmt::Debug> Button<B> {
    pub fn new(id: B) -> Self {
        Self {
            id,
            active_level: Level::High,
            hold_time: None,
            double_time: Duration::from_millis(0),
            debounce: Duration::from_millis(DEFAULT_DEBOUNCE),
            switch_time: Instant::now(),
            level: Level::Low,
            level_debounced: Level::Low,
            held: false,
            double: false,
        }
    }

    #[inline]
    #[must_use]
    pub fn active_level(mut self, active_level: Level) -> Self {
        self.active_level = active_level;
        self.level = !active_level;
        self.level_debounced = self.level;
        self
    }

    #[inline]
    #[must_use]
    pub fn hold_time(mut self, hold_time: Duration) -> Self {
        self.hold_time = Some(hold_time);
        self
    }

    #[inline]
    #[must_use]
    pub fn double_time(mut self, double_time: Duration) -> Self {
        self.double_time = double_time;
        self
    }

    #[inline]
    #[must_use]
    pub fn debounce(mut self, debounce: Duration) -> Self {
        self.debounce = debounce;
        self
    }

    pub(crate) fn held<S, E>(&mut self, sender: &mut S) -> Result<(), S::EmitError>
    where
        S: EventEmitter<E>,
        E: From<ButtonEvent<B>>,
    {
        if !self.held {
            self.held = true;
            self.emit_event(sender)?;
        }
        Ok(())
    }

    pub(crate) fn switched<S, E>(
        &mut self, sender: &mut S, new_level: Level, since_prev_switch: Duration,
    ) -> Result<(), S::EmitError>
    where
        S: EventEmitter<E>,
        E: From<ButtonEvent<B>>,
    {
        // The level changed.
        let pushed = new_level == self.active_level;
        self.level_debounced = new_level;
        self.double = if pushed { since_prev_switch < self.double_time } else { false };
        self.switch_time = Instant::now();
        if new_level == self.active_level {
            // `self.held` is not set to false on release, so a release after
            // hold can easily be handled differently from a release after short
            // push.
            self.held = false;
        }
        self.emit_event(sender)
    }

    pub(crate) fn emit_event<S, E>(&self, sender: &mut S) -> Result<(), S::EmitError>
    where
        S: EventEmitter<E>,
        E: From<ButtonEvent<B>>,
    {
        sender.emit(
            ButtonEvent {
                id: self.id,
                pushed: self.level_debounced == self.active_level,
                held: self.held,
                double: self.double,
            }
            .into(),
        )
    }

    pub(crate) fn handle_holding<S, E>(&mut self, sender: &mut S) -> Result<(), S::EmitError>
    where
        S: EventEmitter<E>,
        E: From<ButtonEvent<B>>,
    {
        let since_prev_switch = self.switch_time.elapsed();
        if self.level != self.level_debounced && since_prev_switch > self.debounce {
            self.switched(sender, self.level, since_prev_switch)?;
        } else if let Some(hold_time) = self.hold_time {
            if self.level_debounced == self.active_level && since_prev_switch >= hold_time {
                self.held(sender)?;
            } else {
                self.held = false;
            }
        }
        Ok(())
    }

    pub(crate) fn handle_pushing<S, E>(
        &mut self, sender: &mut S, level: Level,
    ) -> Result<(), S::EmitError>
    where
        S: EventEmitter<E>,
        E: From<ButtonEvent<B>>,
    {
        let since_prev_switch = self.switch_time.elapsed();
        self.level = level;
        if level != self.level_debounced && since_prev_switch > self.debounce {
            self.switched(sender, level, since_prev_switch)?;
        }
        Ok(())
    }
}
