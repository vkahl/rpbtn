pub trait EventEmitter<E> {
    type EmitError;

    #[allow(clippy::missing_errors_doc)]
    fn emit(&self, event: E) -> Result<(), Self::EmitError>;
}

impl<E> EventEmitter<E> for std::sync::mpsc::Sender<E> {
    type EmitError = std::sync::mpsc::SendError<E>;

    #[inline]
    fn emit(&self, event: E) -> Result<(), Self::EmitError> {
        self.send(event)
    }
}

impl<E> EventEmitter<E> for std::sync::mpsc::SyncSender<E> {
    type EmitError = std::sync::mpsc::SendError<E>;

    #[inline]
    fn emit(&self, event: E) -> Result<(), Self::EmitError> {
        self.send(event)
    }
}
