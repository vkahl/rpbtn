# rpbtn

Using hardware buttons/switches with the raspberrypi made simple

This is a tiny library focused on developer convenience based on the amazing
rppal. If you need more features and more precise control, you should check it
out: https://github.com/golemparts/rppal

## Usage

1. Construct a channel for button events.  
   *Note:* This channel can be used for other types of events as well, if the
   event type implements `From<ButtonEvent<_>>`.
1. Construct a `ButtonWatcher` from the sending end of the channel.
2. Construct multiple `Button`s and register them in the `ButtonWatcher`.
3. Start watching the registered buttons (usually from a background thread).
4. Handle the events received from the channel.

## Features

* Debouncing
* Getting additional events after a specified hold time
* Recognizing double pushes within a specified time
* Using a custom type to identify buttons
* Using a custom event type
* The channels from std work out of the box, but any other channel
  implementation can be used as well by implementin the `EventEmitter<_>` trait
  for its sender.  
  *Note:* Due to the orphan rules, this only works if you define your own event
  type. That could be a struct or enum wrapping `ButtonEvent<_>` and
  implementing `From<ButtonEvent<_>>`.

## Example (stop watch)

*This example is not a complete implementation! It only demonstrates the button
handling.*

``` rust
use std::{sync::mpsc::channel, time::Duration};

use rpbtn::{Button, ButtonEvent, ButtonWatcher, Level};

#[derive(Debug, Clone, Copy)]
enum ButtonId {
    Start,
    Stop, // long press is used for reset
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Construct the channel. The omitted type parameter is the button
    // identifier (in our case the `ButtonId` enum).
    let (btn_send, btn_recv) = channel::<ButtonEvent<_>>();

    // These are the rpi pins the buttons are connected to.
    let pin_start_button = 17;
    let pin_stop_button = 18;

    // Construct the button watcher and the two buttons.
    let btn_watcher = ButtonWatcher::new(btn_send)?
        .button(
            pin_start_button,
            Button::new(ButtonId::Start)
                .active_level(Level::Low) // default is active high
                .debounce(Duration::from_millis(50)), // default is 100ms
        )?
        .button(
            pin_stop_button,
            Button::new(ButtonId::Stop)
                .debounce(Duration::from_millis(50))
                .hold_time(Duration::from_millis(500)),
        )?;

    // Watch buttons in the background. The watcher will automatically break
    // when `btn_recv` is dropped.
    let _join_handle = btn_watcher.watch_background();

    // Handle button events.
    while let Ok(button_event) = btn_recv.recv() {
        match (button_event.id, button_event.pushed, button_event.held) {
            (ButtonId::Start, true, _) => {
                // The start button has been pushed ->
                // Start the stop watch.
            }
            (ButtonId::Stop, true, false) => {
                // The stop button has been pushed ->
                // Stop the stop watch.
            }
            (ButtonId::Stop, true, true) => {
                // The stop button has been held down for 500ms ->
                // Reset the stopwatch.
            }
            _ => {} // Another button event occurred -> Do nothing.
        };
    }

    Ok(())
}
```
