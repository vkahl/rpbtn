use std::{sync::mpsc::channel, time::Duration};

use rpbtn::{Button, ButtonEvent, ButtonWatcher, Level};

#[derive(Debug, Clone, Copy)]
enum ButtonId {
    Start,
    Stop, // long press is used for reset
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Construct the channel. The omitted type parameter is the button
    // identifier (in our case the `ButtonId` enum).
    let (btn_send, btn_recv) = channel::<ButtonEvent<_>>();

    // These are the rpi pins the buttons are connected to.
    let pin_start_button = 17;
    let pin_stop_button = 18;

    // Construct the button watcher and the two buttons.
    let btn_watcher = ButtonWatcher::new(btn_send)?
        .button(
            pin_start_button,
            Button::new(ButtonId::Start)
                .active_level(Level::Low) // default is active high
                .debounce(Duration::from_millis(50)), // default is 100ms
        )?
        .button(
            pin_stop_button,
            Button::new(ButtonId::Stop)
                .debounce(Duration::from_millis(50))
                .hold_time(Duration::from_millis(500)),
        )?;

    // Watch buttons in the background. The watcher will automatically break
    // when `btn_recv` is dropped.
    let _join_handle = btn_watcher.watch_background();

    // Handle button events.
    while let Ok(button_event) = btn_recv.recv() {
        match (button_event.id, button_event.pushed, button_event.held) {
            (ButtonId::Start, true, _) => {
                // The start button has been pushed ->
                // Start the stop watch.
            }
            (ButtonId::Stop, true, false) => {
                // The stop button has been pushed ->
                // Stop the stop watch.
            }
            (ButtonId::Stop, true, true) => {
                // The stop button has been held down for 500ms ->
                // Reset the stopwatch.
            }
            _ => {} // Another button event occurred -> Do nothing.
        };
    }

    Ok(())
}
